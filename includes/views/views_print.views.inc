<?php

/*
 * Implements hook_views_plugins().
 */
function views_print_views_plugins() {
  $path = drupal_get_path('module', 'views_print');  
  $parent_path = drupal_get_path('module', 'views_data_export');
  
  module_load_include('inc', 'views_print', 'views_print.theme.inc');

  $style_defaults = array(
    'path' => $parent_path . '/plugins',
    'parent' => 'views_data_export',
    'theme' => 'views_data_export',
    'theme path' => $parent_path . '/theme',
    'theme file' => 'views_data_export.theme.inc',
    'uses row plugin' => TRUE,
    'uses fields' => TRUE,
    'uses options' => TRUE,
    'type' => 'data_export',
  );
  
  return array(
    'style' => array(
      'views_print_paged_bulk_print' => array(
        'title' => t('Page separated list'),
        'help' => t('Display a printer friendly list with one row per page.'),
        'handler' => 'views_print_plugin_style_paged_bulk_print',
        'export headers' => array('Content-Type' => 'text/html'),
        'export feed type' => 'paged_bulk_print',
        'export feed text' => 'Paged bulk print',
        'export feed file' => '%view.html',
        'additional themes base' => 'views_print_paged_bulk_print',
      ) + $style_defaults,
    ),
    'row' => array(
      'views_print_fields' => array(
        'title' => t('Fields'),
        'help' => t('Displays the fields with an optional template.'),
        'handler' => 'views_plugin_row_fields',
        'theme' => 'views_view_fields',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'data_export',
      ),     
    ),      
  );
}
