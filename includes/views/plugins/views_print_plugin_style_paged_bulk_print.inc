<?php

/*
 * Style plugin: render the view as a paged, printable html page
 */
class views_print_plugin_style_paged_bulk_print extends views_data_export_plugin_style_export {
  function options_definition() {
    // Optional separate stylesheet
    $options['additional_stylesheet'] = array('default' => '');
    return $options;
  }
  
  function options_form(&$form, &$form_state) {    
    $form['additional_stylesheet'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional stylesheet'),
      '#description' => t('Optionally provide the path to an additional stylesheet for this display.'),
      '#default_value' => $this->options['additional_stylesheet']
    );
    
    parent::options_form($form, $form_state);
  }
}
