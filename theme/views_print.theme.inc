<?php

/*
 * Preprocess function: add css to the header of our html export
 */
function template_preprocess_views_print_paged_bulk_print_header(&$vars) {  
  $view = $vars['view'];
  
  // Add our own stylesheet to accomplish page breaks
  drupal_add_css(drupal_get_path('module', 'views_print') . '/theme/views_print.css');  
  
  // Add in a custom style sheet
  if ($path = $view->style_plugin->options['additional_stylesheet']) {
    drupal_add_css($path);
  }
  
  // Add system styles
  $vars['styles'] = drupal_get_css();  
}
