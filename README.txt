Installation instructions:

* Install views_data_export and its dependencies
* Install views_print

Setup:

* Edit a view
* Add a display of type "data export"
* Change the format to "Page separated list"
* Add desired fields
* Theme the output by overriding the pattern "views-data-export--" in the
  standard way. Currently this is the only way to accomplish grids and other 
  special style formatting.
* Attach the data export display to the main display